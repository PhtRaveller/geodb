import os
import shapefile
import urllib2
import zipfile

from default import TEMP_DIR
from default import GB_FTP_SERVER
from default import GB_FTP_USER
from default import TIGER_DIR
from ftplib import FTP


class ShapeFileGrapperException(Exception):
    """Exception class for the shape_file_grabber module"""
    def __init__(self, message):
        Exception.__init__(self, message)


def fetch_shape_files(gb, year, url=None, download=True):
    """ 
    This fuction will return all the shapefiles for a specified
    cartographic boundary and year.
    
    Args:
    :param gb: The geographical boundary
    :type gb: str.
    :param year: The year the information should be from.
    :type year: str.

    Kwargs:
    :param url: Download from this url.
    :type url: str.
    :param download: this toggles if the data should be downloaded or not. Default value is True. Usefull to set to False when debugging.
    :type download: bool.
    
    Resturns:
        :returns: list

    The files found will be downladed to ./tmp and then 
    extracted. The .zip files will be removed immediately
    and the shape files will remain in ./tmp until the
    remove_data() method is called

    """
    # Make sure gb is upper case
    gb = gb.upper()

    # Change work dir to the module path
    script_dir = os.path.dirname(__file__)
    os.chdir(script_dir)
    
    # If a url is specified download that file
    if url:
        if download:
            f = urllib2.urlopen(url)
            filename = gb + '_' + year + '.zip'
            with open(os.path.join(TEMP_DIR, filename), "wb") as zfile:
                    zfile.write(f.read())
        
            zipfile.ZipFile(os.path.join(TEMP_DIR, filename),
                    'r').extractall(TEMP_DIR)
        return gb + '_' + year



    # Connect to FTP
    ftp = FTP(GB_FTP_SERVER, GB_FTP_USER) 
    ftp.cwd(TIGER_DIR + year)

    # Find the directory of the files
    old_dir = ftp.pwd()
    found_files = False
    while True:
        dir_list = ftp.nlst()
        # If the file is in the dir, break
        for item in dir_list:
            if (".zip" in item and gb.lower() in item and
                year in item):
                found_files = True
                break
        if found_files == True:
            break
        else:
            # Change to an appropriate sub directory
            for item in dir_list:
                if ((year in item or gb == item) and '.' not in item):
                    try:
                        ftp.cwd(item)
                    except:
                        raise ShapeFileGrapperException("Error, can't change to: "
                                                        "%s" % item)
                    break
        if old_dir == ftp.pwd():
            raise ShapeFileGrapperException("Error, can't find: "
                                        "%s for %s" % (gb, year))
        old_dir = ftp.pwd()

    shape_files = []
    
    # If there is a collection file, downoad only that
    for item in ftp.nlst():
        if '_us_' in item:
            shape_files = [item]
            break
        else:
            shape_files.append(item)

    # Download the files from the server
    for item in shape_files:
        if download:
            f = open(os.path.join(TEMP_DIR, item),'wb')
            ftp.retrbinary('RETR %s' % item, f.write)
            f.close()

    ftp.close()
    
    # Extract the files
    if download:
        for item in shape_files:
            try:
                zipfile.ZipFile(os.path.join(TEMP_DIR, item), 'r').extractall(TEMP_DIR)
            except:
                raise ShapeFileGrapperException("Error, the zipfile wasn't "
                                                "extracted.")

    return [ item[:-4] for item in shape_files]

def read_shape_file(file_name):
    """ 
    This function will return the shapefile for the
    specified file name

    Keyword arguments:
    file_name -- the name of the shape file

    """

    return shapefile.Reader(os.path.join(TEMP_DIR, file_name))


def remove_shape_files():
    """
    This function will remove the shape files from TEMP_DIR

    """
    script_dir = os.path.dirname(__file__)
    os.chdir(script_dir)
    
    for item in os.listdir(TEMP_DIR):
        os.remove(os.path.join(TEMP_DIR, item))

