import os
import unittest
import shape_file_grabber as sfg

from default import TEMP_DIR


class TestShapeFileGrabber(unittest.TestCase):
    """Tests the shape_file_grabber module"""

    def test_all(self):
        """This function goes through and test a list of possible settings
        and check if the proper answer is recieved. This is done without
        downloading the files."""
        cases = [
               ['STATE', '2012', 'tl_2012_us_state'],
               ['COUNTY', '2012', 'tl_2012_us_county'],
               ['TRACT', '2012', 'tl_2012_01_tract'],
               ['BG', '2012', 'tl_2012_01_bg'],
               ['ZCTA5', '2012', 'tl_2012_us_zcta510'],
               ['STATE', '2011', 'tl_2011_us_state'],
               ['COUNTY', '2011', 'tl_2011_us_county'],
               ['TRACT', '2011', 'tl_2011_01_tract'],
               ['BG', '2011', 'tl_2011_01_bg'],
             # ['ZCTA5', '2011', 'tl_2011_us_zcta510'], # This one dosen't exist
               ['STATE', '2010', 'tl_2010_us_state10'],
               ['COUNTY', '2010', 'tl_2010_us_county10'],
               ['TRACT', '2010', 'tl_2010_01001_tract10'],
               ['BG', '2010', 'tl_2010_01001_bg10'],
               ['ZCTA5', '2010', 'tl_2010_us_zcta510'],
               ['CSA', '2010', 'tl_2010_us_csa10'],
                ]
        for item in cases:
            actual_iterable = sfg.fetch_shape_files(item[0], item[1], download=False)[0]
            excpected_iterable = item[2]
            self.assertEqual(actual_iterable, excpected_iterable)

    def test_2011_zcta5(self):
        # The ZCTA5 does not exist, therefore, an exception should be raised.
        with self.assertRaises(sfg.ShapeFileGrapperException):
            sfg.fetch_shape_files('ZCTA5', '2011', download=False)

    def test_url_download(self):
        # This tests that its possible to download via a url address
        actual_iterable = sfg.fetch_shape_files('WORLD',
                '2012', url='https://dl.dropbox.com/u/23378302/WORLD_2012.txt.zip',
                download=True)
        excpected_iterable = 'WORLD_2012'
        self.assertEqual(actual_iterable, excpected_iterable)

    def test_removal_of_file(self):
        """Tests the removal function by first creating a file in the tmp dir 
        and then removes it""" 
        open(os.path.join(TEMP_DIR, 'test.txt'),'w').close()
        if len(os.listdir(TEMP_DIR)) < 1:
            raise "Test not working properly"
        sfg.remove_shape_files()
        self.assertEqual(len(os.listdir('tmp')), 0)

