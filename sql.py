#! /usr/bin/env python
"""
This module will create and populate an DQL database
"""
from db_utils import GeoUnits, Polygons, CoordinatesRaw
from db_utils import engine
import sqlalchemy

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, String

from sqlalchemy import ForeignKey
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship, backref

Session = sessionmaker(bind=engine)
session = Session()

def update_db(inp):
    """ """
    if inp == None: return None

    fields_tab_geo_units = {
        'blockcode'        : None, 
        'zipcode5'        : None, 
        'name'            : None,
        'districtcode'    : None,
        'countycode'      : None,
        'tractcode'       : None, 
        'lsadname'        : None,
        'statecode'       : None,
        'subdivcode'      : None,
        'lsadcode'        : None,
        'zipcode3'        : None,
    }

# Structure of 'geometry': top level -- polygons
#                          mid level -- points of a polygon
#                          low level -- longitude,latitude of a point.

    tab_geo_units = GeoUnits()
    session.add(tab_geo_units)
    session.commit() # Must be present to make 'geo_unit_id' available in the next steps

    for field in fields_tab_geo_units:
        if inp.has_key ( field ) : setattr( tab_geo_units, field, inp[field] )

    if inp.has_key ( 'geometry' ) : 
        polygons = inp [ 'geometry' ]
        geo_unit_ipoly = 1
        for polygon in polygons:
            tab_polygons = Polygons()
            tab_polygons.geo_unit_id = tab_geo_units.geo_unit_id
            tab_polygons.geo_unit_ipoly = geo_unit_ipoly
            num_points = len(polygon)
            tab_polygons.num_points = num_points
            session.add(tab_polygons)
            session.commit()  # Must be present to make 'polygon_id' value in the next cycle
            geo_unit_ipoly = geo_unit_ipoly + 1
        for point in polygon:
            tab_coordinates_raw = CoordinatesRaw()
            tab_coordinates_raw.polygon_id = tab_polygons.polygon_id
            tab_coordinates_raw.longitude = point[0]
            tab_coordinates_raw.latitude =  point[1]
            session.add(tab_coordinates_raw)
        session.commit() 
    session.flush()

def query_db(what):
    from sqlalchemy import join
    from ast import literal_eval

    return_list=list()
    if what == 'state':
        filter_state = "geo_units.statecode IS NOT NULL"
        filter_county = "geo_units.countycode IS NULL"
        filter_tract = "geo_units.tractcode IS NULL"
        filter_block = "geo_units.blockcode IS NULL"
        filter_zip5 = "geo_units.zipcode5 IS NULL"
    elif what == 'county':
        filter_state = "geo_units.statecode IS NOT NULL"
        filter_county = "geo_units.countycode IS NOT NULL"
        filter_tract = "geo_units.tractcode IS NULL"
        filter_block = "geo_units.blockcode IS NULL"
        filter_zip5  = "geo_units.zipcode5 IS NULL"
    elif what == 'tract':
        filter_state = "geo_units.statecode IS NOT NULL"
        filter_county = "geo_units.countycode IS NOT NULL"
        filter_tract = "geo_units.tractcode IS NOT NULL"
        filter_block = "geo_units.blockcode IS NULL"
        filter_zip5 = "geo_units.zipcode5 IS NULL"
    elif what == 'block':
        filter_state = "geo_units.statecode IS NOT NULL"
        filter_county = "geo_units.countycode IS NOT NULL"
        filter_tract = "geo_units.tractcode IS NOT NULL"
        filter_block = "geo_units.blockcode IS NOT NULL"
        filter_zip5 = "geo_units.zipcode5 IS NULL"
    elif what == 'zipcode5':
        filter_state = "geo_units.statecode IS NOT NULL"
        filter_county = "geo_units.countycode IS NULL"
        filter_tract = "geo_units.tractcode IS NULL"
        filter_block = "geo_units.blockcode IS NULL"
        filter_zip5 = "geo_units.zipcode5 IS NOT NULL"
    else:
        return None

    Q_geo_unit_ipoly = session\
                      .query(Polygons.polygon_id)\
                      .outerjoin(GeoUnits)\
                      .filter( filter_state, filter_county, filter_tract,
                               filter_block, filter_zip5)

    if Q_geo_unit_ipoly.count() == 0:
        return None
    for state in Q_geo_unit_ipoly.all():
        polygons = state[:]
        for polygon in polygons:
            print polygon
            Q_coordinates = session\
                          .query(CoordinatesRaw.longitude,CoordinatesRaw.latitude)\
                          .outerjoin(Polygons)\
                          .outerjoin(GeoUnits)\
                          .filter(Polygons.polygon_id==polygon)\
                          .filter( filter_state, filter_county, filter_tract,
                                   filter_block, filter_zip5 )
            longitude_latitude_pairs = Q_coordinates.all()[:]
            print longitude_latitude_pairs
            return_list.append(longitude_latitude_pairs)

    return return_list