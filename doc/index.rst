.. GeoDB documentation master file, created by
   sphinx-quickstart on Thu Mar 21 21:06:24 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GeoDB's documentation!
=================================
This project was created as a part of the *Workshop on Computer Programming and Advanced Tools for Scientific Research Work & Quantum ESPRESSO Developer Training*


.. toctree::
   :maxdepth: 3

    geodb
        
* GeoDB

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

