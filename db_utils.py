from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, String
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

from default import db_config as _db_config

def _get_engine():
    engine = create_engine(_db_config)
    return engine

engine = _get_engine()
_DB = declarative_base()

class GeoUnits(_DB):
    __tablename__ = 'geo_units'
    geo_unit_id = Column('geo_unit_id', Integer(11), nullable=False,
                    primary_key=True, default=None, autoincrement=True)
    blockcode = Column('blockcode', Integer(4), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    zipcode5 = Column('zipcode5', Integer(5), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    name = Column('name', String(100), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    districtcode = Column('districtcode', Integer(5), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    countycode = Column('countycode', Integer(3), nullable=True, 
                primary_key=False,  default=None, autoincrement=False)
    tractcode = Column('tractcode', Integer(6), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    lsadname = Column('lsadname', String(100), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    statecode = Column('statecode', Integer(2), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    subdivcode = Column('subdivcode', Integer(5), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    lsadcode = Column('lsadcode', Integer(2), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    zipcode3 = Column('zipcode3', Integer(3), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    children = relationship( "Polygons", backref="geo_units")

    def __init__(self):
        """"""

class Polygons(_DB):
    __tablename__='polygons'
    polygon_id = Column('polygon_id', Integer(11), nullable=False,
                primary_key=True, default=None, autoincrement=True)
    geo_unit_id = Column('geo_unit_id', Integer(11),
                ForeignKey('geo_units.geo_unit_id'), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    geo_unit_ipoly = Column('geo_unit_ipoly', Integer(11), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    num_points = Column('num_points', Integer(11),  nullable=True,
                primary_key=False, default=None, autoincrement=False)
    children = relationship( "CoordinatesRaw", backref="polygons")
    def __init__(self):
        """"""

class CoordinatesRaw(_DB):
    """ """
    __tablename__='coordinates_raw'
    coordinate_id = Column('coordinate_id', Integer(11), nullable=False,
                primary_key=True, default=None, autoincrement=True)
    polygon_id = Column('polygon_id', Integer(11),
                ForeignKey('polygons.polygon_id'), nullable=True,
                primary_key=False, default=None, autoincrement=False)
    longitude = Column('longitude', Float, nullable=True,
                primary_key=False, default=None, autoincrement=False)
    latitude = Column('latitude', Float, nullable=True,
                primary_key=False, default=None, autoincrement=False)
    def __init__(self):
        """"""

_DB.metadata.create_all(engine)