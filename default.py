"""
Default settings live here. This module tries to import conf.py to perform custom setup.
For future flexibility:)
author: glib.ivashkevych@gmail.com
"""
# FTP settings:
_GB_FTP_SERVER = 'ftp2.census.gov'
_GB_FTP_USER = 'anonymous'
_TIGER_DIR = 'geo/tiger/TIGER'

# Directory for temporary storage of downloaded shape files:
_TEMP_DIR = 'tmp/'

PARSING_DEBUG = True

#For ref. see 2010 TIGER/Line Shapefiles Technical Documentation
_census_2010_notation = {
    'GEOID10':'gov_geo_unit_id', #unit identifier, combination of state FIPS with corrsponding unit FIPS code 
    'STATEFP10':'statecode', #2010C state FIPS code
    'NAME10':'name', #state name
    'COUNTYFP10':'countycode', #2010C county FIPS code
    'LSAD10':'lsadcode', #legal/statistical area description code (state level)
    'TRACTCE10':'tractcode', #2010C tract code
    'NAMELSAD10':'lsadname', #2010C translated legal/statistical area description code and blockgroup number (?)
    'BLKGRPCE10':'blockcode', #2010C block group number
    'COUSUBFP10':'subdivcode', #2010C county subdivision code
    'TRSUBCE10':'subdivcode', #2010C tribal subdivision code
    'ZCTA5CE10':'zipcode5', #2010C 5-digit ZIP Code tabulation area code
}

_exclusion_list = ['DeletionFlag']
_boundaries = ['state', 'county', 'tract', 'bg', 'zcta5']
_year = '2010'
_db_engine = 'sqlite'
_db_host = 'geodata.db'
_db_username = ''
_db_passwd = ''

def _construct_db_config(dbe, dbh, dbu, dbp):
    if dbe == "sqlite":
        return "%s:///%s" % (dbe, dbh)
    else:
        return "%s://%s:%s@%s" % (dbe, dbu, dbp, dbh)

_plotting_list = ['state']
_plot_elevation = 0.

#Very rough parsing of setting here - trying to figure out whether there
#are some custom settings in conf.py.
try:
    import conf
    custom_conf_dir = dir(conf)
    if 'notation' in custom_conf_dir:
        notation = conf.notation
    else:
        notation = _census_2010_notation

    if 'exclusion_list' in custom_conf_dir:
        exclusion_list = conf.exclusion_list
    else:
        exclusion_list = _exclusion_list

    if 'boundaries' in custom_conf_dir:
        boundaries = conf.boundaries
    else:
        boundaries = _boundaries

    if 'year' in custom_conf_dir:
        year = conf.year
    else:
        year = _year

    if 'ftp_server' in custom_conf_dir:
        GB_FTP_SERVER = conf.ftp_server
    else:
        GB_FTP_SERVER = _GB_FTP_SERVER

    if 'ftp_dir' in custom_conf_dir:
        TIGER_DIR = conf.ftp_dir
    else:
        TIGER_DIR = _TIGER_DIR

    if 'temp_dir' in custom_conf_dir:
        TEMP_DIR = conf.temp_dir
    else:
        TEMP_DIR = _TEMP_DIR

    if 'db_engine' in custom_conf_dir:
        db_engine = conf.db_engine
    else:
        db_engine = _db_engine

    if 'db_host' in custom_conf_dir:
        db_host = conf.db_host
    else:
        db_host = _db_host

    if 'db_username' in custom_conf_dir:
        db_username = conf.db_username
    else:
        db_username = _db_username

    if 'db_passwd' in custom_conf_dir:
        db_passwd = conf.db_passwd
    else:
        db_passwd = _db_passwd

    if 'plotting_list' in custom_conf_dir:
        plotting_list = conf.plotting_list
    else:
        plotting_list = _plotting_list

    if 'plot_elevation' in custom_conf_dir:
        plot_elevation = conf.plot_elevation
    else:
        plot_elevation = _plot_elevation

    db_config = _construct_db_config(db_engine, db_host, db_username, db_passwd,)
    GB_FTP_USER = _GB_FTP_USER

except ImportError:
    notation = _census_2010_notation
    exclusion_list = _exclusion_list
    boundaries = _boundaries
    year = _year
    GB_FTP_SERVER = _GB_FTP_SERVER
    TIGER_DIR = _TIGER_DIR
    TEMP_DIR = _TEMP_DIR
    GB_FTP_USER = _GB_FTP_USER
    db_config = _construct_db_config(_db_engine, _db_host, _db_username, _db_passwd)
    plotting_list = _plotting_list
    plot_elevation = _plot_elevation

def _translate_2010C_notation_to2000():
    """Translates 2010 Census notation to 2000 notation. Not sure if this is
    appropriate module for this function."""
    result = {}
    for k,v in _census_2010_notation.iteritems():
        result[k.replace('10','00')] = v
    return result