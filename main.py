#!/usr/bin/python

from shape_file_grabber import fetch_shape_files, read_shape_file, remove_shape_files
from parsing import parse_shape
from sql import update_db, query_db
from polygons_to_kml import output_kml
import default

def main():
    fetched_files = []
    #Fetching files

    # for boundary in default.boundaries:
    #     print "Starting fetching data for %s, year %s..." % (boundary, default.year)
    #     fetched_files.extend(fetch_shape_files(boundary, default.year, download=True))
    # #Opening and parsing fetched files
    # for fetched_file in fetched_files:
    #     shpf_obj = read_shape_file(fetched_file)
    #     shape_data = parse_shape(shpf_obj)
    #     #Saving parsed data to DB
    #     for shape in shape_data:
    #         update_db(shape)
    #Querying DB and plotting data 

    for plot in default.plotting_list:
        shapes = query_db(plot)
        kml_name = "Verification of %s boundaries" % plot
        kml_filename = "geo_db_%s_verification" % plot
        output_kml(kml_name, kml_filename, shapes, default.plot_elevation)

if __name__ == "__main__":
    main()