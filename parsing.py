"""
parsing.py
Provides functionality for parsing individual shapefile objects contructed with
shapefile.Reader(shpfilename). Main function is parse_shape(shpf_obj), which
returns a list of dictionaries with attributes and geometry data of each record
in shapefile.
author: glib.ivashkevych@gmail.com
Not tested for compatibility with Python 3.x
"""
import shapefile
from default import notation as _notation
from default import exclusion_list as _exclusion_list
from default import PARSING_DEBUG

def parse_shape(shpf_obj):
	"""Contructs a list of distionaries for insertion to database. Extracts
	attributes for each shape according to a dictionary called notation. This
	dictionary can be default or provided by user (in conf.py) and contains
	a mapping between TIGER/Line attributes names and actual names
	of DB columns.

	If something went wrong (e.g. received None as argument or cannot parse
	shapefile for any reason), this function will return None. 

	shpf_obj -- a shapefile object to parse.
	"""
	if shpf_obj is None:
		return None
	try:
		parsed_shapes = []
		fields = _get_field_names(shpf_obj.fields)
		records = shpf_obj.records()
		shapes = shpf_obj.shapes()
		if PARSING_DEBUG:
			print "Processing %s... \nGot %i records with %i shapes (must be equal)" % \
										(shpf_obj.shp.name, len(records), len(shapes))
		for i in range(len(records)):
			current_record = _get_record_data(fields, records[i])
			if shapes[i].parts != [0]:
				current_record['geometry'] = _transform_geometry(shapes[i])
			else:
				current_record['geometry'] = [shapes[i].points]
			parsed_shapes.append(current_record)
	except:
		print "Smth went wrong while parsing " + str(shpf_obj) + ". Is it a correct shapefile?\n"
		return None

	return parsed_shapes if parsed_shapes != [] else None

def _get_field_names(fields):
	"""Returns list of field names from given list with fields data.
	Pass shapefile.Reader(...).fields here."""
	return [field[0] for field in fields if field[0] not in _exclusion_list]

def _get_record_data(field_names, record, notation=_notation):
	"""Returns a dictionary with record data per shape. Geo data itself is not included."""
	result = {}
	for field in field_names:
		if field in notation:
			result[notation[field]] = record[field_names.index(field)]
	return result if result != {} else None

def _transform_geometry(shape):
	"""Returns list of polygon parts for a given shape."""
	result = []
	start_point_index = shape.parts[0]
	for end_point_index in shape.parts[1:]:
		result.append(shape.points[start_point_index:end_point_index])
		start_point_index = end_point_index
	result.append(shape.points[start_point_index:])
	return result