"""
Polygons to KML converter.

The set of functions on this module help to convert a list structure that
contains a list of polygons with their respective coordinate boundaries to
a KML file which can be visualized with Google Earth.

Since concatenation of strings is very slow, the different sections of the
KML file are not written at the same time. Blocks that can be written 
sequentially to the output file, are stored as strings in variables.
To keep the size of the output kml file small, white spaces are removed, 
and the file ends up being not very human readable.

The polygon element lacks some flexibility on the visualization options, 
it was made to just provide a quick visualization, and can be easily 
improved.

Author: Tristana Sondon <tsondon@gmail.com>
Date: March 2013
Copyright: Tristana Sondon, 2013.

"""

###########################################################################
#    Set basic KML string variables that do not depend on the data.
###########################################################################

# Global Header for the KML file
header = '<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2"><Document>'

# Several Polygon Styles to choose from (to avoid the default ugly gray polygon)
# Styles available right now: transGreenPoly, transBluePoly, transRedPoly
style = '<Style id="transGreenPoly"><LineStyle><width>1.5</width></LineStyle><PolyStyle><color>7d00ff00</color></PolyStyle></Style><Style id="transBluePoly"><LineStyle><width>1.5</width></LineStyle><PolyStyle><color>7dff0000</color></PolyStyle></Style><Style id="transRedPoly"><LineStyle><width>1.5</width></LineStyle><PolyStyle><color>7d0000ff</color></PolyStyle></Style>'

# Begin a polygon block (not very flexible, can be improved in the future to include
# a better way to combine the options)
begin_polygon = '<Placemark><name>Relative</name><visibility>1</visibility><styleUrl>#transBluePoly</styleUrl><Polygon><tessellate>1</tessellate><altitudeMode>relativeToGround</altitudeMode><outerBoundaryIs><LinearRing><coordinates>'

# with the option clampToGround the polygons are empty (do I need a different style?)
# I leave here the string for future test, but it is not used now.
begin_polygon_clamp = '<Placemark><name>Relative</name><visibility>1</visibility><styleUrl>#transBluePoly</styleUrl><Polygon><tessellate>1</tessellate><altitudeMode>clampToGround</altitudeMode><outerBoundaryIs><LinearRing><coordinates>'

# End a polygon block
end_polygon = '</coordinates></LinearRing></outerBoundaryIs></Polygon></Placemark>' 

# Close KML structure
footer = "</Document></kml>"
    

###########################################################################
#  Function that provides the conversion Poly Structure to KML
###########################################################################

def output_kml(kml_name, filename, polystructure, elev):
    """This function takes a polygon structure file and outputs a KML file.
    
    TODO: explain arguments, describe the datastructure better 
    (It can still vary depending on database queries).

    Data structure that is converted to KML:

    struct = [[[la, lo], [..]  ],[], ..[]  ]

    where
        struct_to_verif :  struc = [ poly1, poly2, ..., poly_n ]
        polygon         :  poly  = [ coor1, coor2, ...,  coor_n ]  
        coordinates     :  coor  = [ la, lo ]
    """

    # Set GE name
    global_name = "<name>" + kml_name + "</name>"

    # Open KML file
    try:
        kmlfile = file(filename + '.kml', 'w') 
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

    # Write Header
    kmlfile.write(header + style + global_name)

    # Convert each polygon to a string and write a polygon block
    for p in polystructure:
        polyg = ""
        for c in p:
            polyg = polyg +  str(c[0]) + "," + str(c[1]) + "," + str(elev) + " "
        kmlfile.write(begin_polygon + polyg + end_polygon)

    # Write closing KML tags
    kmlfile.write(footer)

    # Close KML file
    kmlfile.close()


if __name__ == "__main__":

    # Manual construction of a polystructure to test conversion
    # This should be moved to other files to perform a unit test

    coor1 = [13.714667,45.713222]
    coor2 = [13.714667,45.714222]
    coor3 = [13.715267,45.714222]
    coor4 = [13.714967,45.713222]
    coor5 = [13.714667,45.713222]

    poly1 = []
    poly1.append(coor1)
    poly1.append(coor2)
    poly1.append(coor3)
    poly1.append(coor4)
    poly1.append(coor5)

    coor6 = [13.715667,45.713222]
    coor7 = [13.715667,45.714222]
    coor8 = [13.716267,45.714222]
    coor9 = [13.715967,45.713222]
    coor10 = [13.715667,45.713222]

    poly2 = []
    poly2.append(coor6 )
    poly2.append(coor7 )
    poly2.append(coor8 )
    poly2.append(coor9 )
    poly2.append(coor10)

    structpoly = []
    structpoly.append(poly1)
    structpoly.append(poly2)

    # output kml file (.kml extension will be added automatically)
    filename = "geo_db_verification"

    # Name that appears on the left panel in Google Earth
    kml_name = "Geo Boundaries Validation"

    # elevation to give as a third "coordinate" to Google Earth
    elev = 20.

    # structpoly now has two polygons with four coordinates each:
    # the next call generates the .kml file
    output_kml(kml_name, filename, structpoly, elev)


