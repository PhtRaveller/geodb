"""
This module can contain custom settings for main.py. Default values are in default.py
and correspond to all boundaries (state, county, tract, blockgroup, zip) for 2010.

======================
You can set custom list of boundaries like this:

boundaries = <list of boundaries you want to fetch, parse, save and verify>.

If no boundaries provided, default is fetch all - state, county, tract, blockgroup,
zip code tabulation areas.

======================
To provide custom notation, povide this:

notation = {
	'<attribute name from T/L shapefile>':'<DB column name>',
	...
}

If no custom notation provided, defaults will be used. See default.py.

======================
To provide a year (which is 2010 by default):

year = <whatever year you want> 

======================
To provide custom exclusion_list (items from this list will be ignored when parsing
attributes of shapefile):

exclusion_list = ['<whatever you want to exclude from parsing>']

This doesn't affect performance, mainly, and is used to drop technical attributes
in shapefile (e.g. like 'DeletionFlag').
"""

boundaries = ['state']
db_engine = 'mysql'
db_username = 'geodb'
db_passwd = 'geopwd'
db_host = 'localhost/geodb'
plot_elevation = 50.